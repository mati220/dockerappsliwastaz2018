package pl.webnetix.dockerapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;

/*
 * @author      Mateusz Śliwa
 */
@SpringBootApplication
@EnableDiscoveryClient
public class DockerAppApplication {

    private static String[] args;
    private static ConfigurableApplicationContext context;


    public static void main(String[] args) {
        DockerAppApplication.args = args;
        DockerAppApplication.context = SpringApplication.run(DockerAppApplication.class, args);
    }


    public static void restart() {
        // close previous context
        context.close();

        // and build new one
        DockerAppApplication.context = SpringApplication.run(DockerAppApplication.class, args);

    }
}
