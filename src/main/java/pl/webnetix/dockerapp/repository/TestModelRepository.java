package pl.webnetix.dockerapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.webnetix.dockerapp.TestModel;

@Repository
public interface TestModelRepository extends JpaRepository<TestModel, Long> {
}
