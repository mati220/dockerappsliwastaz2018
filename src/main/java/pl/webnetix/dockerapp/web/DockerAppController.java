package pl.webnetix.dockerapp.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.webnetix.dockerapp.TestModel;
import pl.webnetix.dockerapp.repository.TestModelRepository;

/*
 * @author      Mateusz Śliwa
 */
@Controller
public class DockerAppController {

    @Autowired
    private TestModelRepository testModelRepository;

    private static Logger LOGGER = LoggerFactory.getLogger(DockerAppController.class);

    @RequestMapping(value = "/getMessage", method = RequestMethod.GET)
    public ResponseEntity getMessage() {
        //double a = 2/0;
        LOGGER.info("Get Message");
        return new ResponseEntity("Message", HttpStatus.OK);
    }

    @RequestMapping(value = "/getModels", method = RequestMethod.GET)
    public ResponseEntity getModels() {
        LOGGER.info("Get Models");
        return new ResponseEntity(testModelRepository.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/addModel", method = RequestMethod.POST)
    public ResponseEntity addModel(@RequestBody TestModel model) {
        LOGGER.info("Add Model: " + model);
        testModelRepository.save(model);
        return new ResponseEntity(HttpStatus.CREATED);
    }

}
