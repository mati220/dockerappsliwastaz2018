package pl.webnetix.dockerapp.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import pl.webnetix.dockerapp.DockerAppApplication;


/*
 * @author      Mateusz Śliwa
 */
@Controller
public class AppOperation {


    @GetMapping("/restart")
    @ManagedOperation(description = "reset")
    public ResponseEntity restart() {
        Thread restartThread = new Thread(() -> {
//            try {
                //Thread.sleep(1000);
                DockerAppApplication.restart();
//            } catch (InterruptedException ignored) {
//            }
        });
        restartThread.setDaemon(false);
        restartThread.start();
        return new ResponseEntity("Restart Application",HttpStatus.OK);
    }
}
