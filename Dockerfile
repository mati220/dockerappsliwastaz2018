FROM java:8
VOLUME /tmp
ARG JAR_FILE
COPY ${JAR_FILE} docker-app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/docker-app.jar"]
CMD ["-start"]
